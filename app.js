var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');

const postsRouter = require('./routes/posts');
const userRouter = require('./routes/users');

var mongoose = require('mongoose');

var app = express();

mongoose.connect("mongodb+srv://stas:F2QuSVqMC8fg@mean-course-nwa26.azure.mongodb.net/node-angular", { useNewUrlParser: true, useCreateIndex: true })
    .then(
        () => { console.log('\x1b[1m', 'Successfuly connected to DataBase:)\n', '\x1b[0m') }
    )
    .catch(() => {
        console.error('\x1b[31m', 'Connection to DataBase failed because of:', err.errmsg, '\x1b[0m');
        console.error('See also full error message: \n', err)
    });

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use("/images", express.static(path.join("public/images")));

// setup api requests
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, PUT, DELETE, OPTIONS');
    next();
});

//mean course
app.use('/api/posts', postsRouter);
app.use('/api/users', userRouter);

module.exports = app;
